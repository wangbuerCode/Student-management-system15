package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.MessageDao;
import cn.itcast.jsjwl.domain.Message;
import cn.itcast.jsjwl.domain.Student;
import cn.itcast.jsjwl.utils.PageModel;

public class MessageService {

	public  PageModel findMessageWithPage(int currentNum, Student stu) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		MessageDao MessageDao=new MessageDao();
		int totalRecords=MessageDao.findTotalRecordsByStuId(stu);
		PageModel pm=new PageModel(currentNum,totalRecords,5);
		//为PageModel设置集合
		List<Message> list=MessageDao.findMessageWithPage(pm.getStartIndex(),pm.getPageSize(),stu);
		pm.setList(list);
		//为PageModel设置url
		pm.setUrl("MessageServlet?method=findMessageWithPage");
		//返回PageModel对象
		return pm;
	}

	public void addMessage(Message msg) throws SQLException {
		MessageDao MessageDao=new MessageDao();
		MessageDao.addMessage(msg);
	}
	public PageModel findMessagesWithPageByTeacher(int currentPageNum) throws SQLException {
		//1_创建pagemodel对象，计算分页参数信息
		MessageDao MessageDao = new MessageDao();
		int totalRecords = MessageDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <Message> list = MessageDao.findMessagesWithPageByTeacher(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("MessageServlet?method=findMessagesWithPageByTeacher");
		return pm;
	}

	public void replayMessage(Message replaymsg,String id) throws SQLException {
		MessageDao MessageDao = new MessageDao();
		MessageDao.replayMessage(replaymsg,id);
	}

	public Message findMessageBymessageid(String messageId) throws SQLException {
		MessageDao MessageDao=new MessageDao();
		return MessageDao.findMessageBymessageid(messageId);
	}

	public PageModel findMessageWithPageByAdmin(int currentPageNum) throws SQLException {
		//1_创建pagemodel对象，计算分页参数信息
				MessageDao MessageDao = new MessageDao();
				int totalRecords = MessageDao.findTotalRecords();
				PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
				//2_为Page'Model设置集合（当前页的留言信息）
				List <Message> list = MessageDao.findMessageWithPageByAdmin(pm.getStartIndex(), pm.getPageSize());
				pm.setList(list);
				//3_为pagemodel设置url
				pm.setUrl("MessageServlet?method=findMessageWithPageByAdmin");
				return pm;
	}

	public void messageDel(String mId) throws SQLException {
		MessageDao MessageDao=new MessageDao();
		MessageDao.messageDel(mId);
		
	}



}
