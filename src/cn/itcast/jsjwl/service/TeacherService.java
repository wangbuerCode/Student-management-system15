package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.TeacherDao;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.utils.PageModel;

public class TeacherService {
     public Teacher teacherLogin(String um,String up) throws SQLException {
    	 TeacherDao TeacherDao=new TeacherDao();
    	 return TeacherDao.teacherLogin(um,up);
 
     }

	public PageModel findTeachersWithPage(int currentNum) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
				int totalRecords=TeacherDao.findTotalRecords();
				PageModel pm=new PageModel(currentNum,totalRecords,5);
				//2为PageModel关联集合集合中存放的就是当前页的资料信息
				//调用Dao层查看当前分页信息
				TeacherDao TeacherDao=new TeacherDao();
				List<Teacher>list=TeacherDao.findTeacherWithPage(pm.getStartIndex(),pm.getPageSize());
				pm.setList(list);
				//3为PageModel关联Url属性
				pm.setUrl("TeacherServlet?method=findTeachersWithPage");
				return pm;
	}

	public void addTeacher(Teacher teacher) throws SQLException {
		TeacherDao TeacherDao=new TeacherDao();
		TeacherDao.addTeacher(teacher);	
	}

	public void delTeacherById(String tId) throws SQLException {
		TeacherDao TeacherDao=new TeacherDao();
		TeacherDao.delTeacherById(tId);	
		
	}

	public PageModel findTeachersWithPageByStu(int currentNum) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
		int totalRecords=TeacherDao.findTotalRecords();
		PageModel pm=new PageModel(currentNum,totalRecords,5);
		//2为PageModel关联集合集合中存放的就是当前页的资料信息
		//调用Dao层查看当前分页信息
		TeacherDao TeacherDao=new TeacherDao();
		List<Teacher>list=TeacherDao.findTeachersWithPageByStu(pm.getStartIndex(),pm.getPageSize());
		pm.setList(list);
		//3为PageModel关联Url属性
		pm.setUrl("TeacherServlet?method=findTeachersWithPageByStu");
		return pm;
	}

	public void updateTeacher(Teacher t) throws SQLException {
		TeacherDao TeacherDao=new TeacherDao();
		TeacherDao.updateTeacher(t);
	}


	
}
