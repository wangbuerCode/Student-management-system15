package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.TeachingNoticeDao;
import cn.itcast.jsjwl.domain.TeachingNotice;
import cn.itcast.jsjwl.utils.PageModel;

public class TeachingNoticeService {

	public PageModel findTeachingNoticeWithPage(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
		TeachingNoticeDao TeachingNoticeDao = new TeachingNoticeDao();
		int totalRecords = TeachingNoticeDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <TeachingNotice> list = TeachingNoticeDao.findTeachingNoticeWithPage(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("TeachingNoticeServlet?method=findTeachingNoticeWithPage");
		return pm;
	}

	public void addTeachingNotice(TeachingNotice teachingNotice) throws SQLException {
		TeachingNoticeDao TeachingNoticeDao=new TeachingNoticeDao();
		TeachingNoticeDao.addTeachingNotice(teachingNotice);
		
	}

	public PageModel findTeachingNoticeWithPageByAdmin(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
		TeachingNoticeDao TeachingNoticeDao = new TeachingNoticeDao();
		int totalRecords = TeachingNoticeDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <TeachingNotice> list = TeachingNoticeDao.findTeachingNoticeWithPageByAdmin(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("TeachingNoticeServlet?method=findTeachingNoticeWithPageByAdmin");
		return pm;
	}

	public void deleteTeachingNoticeByAdmin(String tnId) throws SQLException {
		TeachingNoticeDao TeachingNoticeDao = new TeachingNoticeDao();
		TeachingNoticeDao.deleteTeachingNoticeByAdmin(tnId);
		
	}

	public PageModel findTeachingNoticeByStu(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
		TeachingNoticeDao TeachingNoticeDao = new TeachingNoticeDao();
		int totalRecords = TeachingNoticeDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <TeachingNotice> list = TeachingNoticeDao.findTeachingNoticeByStu(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("TeachingNoticeServlet?method=findTeachingNoticeByStu");
		return pm;
	}

	public TeachingNotice findTeachingNoticeById(String tId) throws SQLException {
		//调用DAO层根据sID获取对象
		TeachingNoticeDao TeachingNoticeDao = new TeachingNoticeDao();
		return TeachingNoticeDao.findTeachingNoticeById(tId);
	}

}
