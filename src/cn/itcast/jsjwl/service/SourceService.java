package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.SourceDao;
import cn.itcast.jsjwl.domain.Source;
import cn.itcast.jsjwl.utils.PageModel;

public class SourceService {

	public List<Source> findPreSource() throws SQLException {
		// 调用DAO层功能，返回一个存储着ziliao对象的集合
		SourceDao SourceDao=new SourceDao();
		return SourceDao.findPreSource();

	}

	public PageModel findSourceWithPage(int num) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
		int totalRecords=SourceDao.findTotalRecords();
		PageModel pm=new PageModel(num,totalRecords,5);
		//2为PageModel关联集合集合中存放的就是当前页的资料信息
		//调用Dao层查看当前分页信息
		SourceDao SourceDao=new SourceDao();
		List<Source>list=SourceDao.findSourceWithPage((num-1)*5,5);
		pm.setList(list);
		//3为PageModel关联Url属性
		pm.setUrl("SourceServlet?method=findSourceWithPage");
		return pm;
	}

	public Source findSourceBySid(String sId) throws SQLException {
		//调用DAO层根据sID获取对象
		SourceDao SourceDao=new SourceDao();
		return SourceDao.findSourceBySid(sId);
	
	}

	public PageModel findSourceWithPageByTeacher(int currentPageNum) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
				int totalRecords=SourceDao.findTotalRecords();
				PageModel pm=new PageModel(currentPageNum,totalRecords,5);
				//2为PageModel关联集合集合中存放的就是当前页的资料信息
				//调用Dao层查看当前分页信息
				SourceDao SourceDao=new SourceDao();
				List<Source>list=SourceDao.findSourceWithPageByTeacher(pm.getStartIndex(),pm.getPageSize());
				pm.setList(list);
				//3为PageModel关联Url属性
				pm.setUrl("SourceServlet?method=findSourceWithPageByTeacher");
				return pm;
	}

	public void deleteSourceByTeacher(String sId) throws SQLException {
		//调用DAO层
		SourceDao SourceDao=new SourceDao();
		SourceDao.deleteSourceByTeacher(sId);	
	}
	public void addSource(Source source) throws SQLException {
		SourceDao SourceDao=new SourceDao();
		SourceDao.addSource(source);
	}

	public void deleteSourceByAdmin(String sId) throws SQLException {
		
		//调用DAO层
				SourceDao SourceDao=new SourceDao();
				SourceDao.deleteSourceByAdmin(sId);	
	}

	public PageModel findsourceWithPageByAdmin(int num) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
				int totalRecords=SourceDao.findTotalRecords();
				PageModel pm=new PageModel(num,totalRecords,5);
				//2为PageModel关联集合集合中存放的就是当前页的资料信息
				//调用Dao层查看当前分页信息
				SourceDao SourceDao=new SourceDao();
				List<Source>list=SourceDao.findsourceWithPageByAdmin((num-1)*5,5);
				pm.setList(list);
				//3为PageModel关联Url属性
				pm.setUrl("SourceServlet?method=findsourceWithPageByAdmin");
				return pm;
	}


}
