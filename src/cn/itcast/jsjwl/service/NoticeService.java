package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.NoticeDao;
import cn.itcast.jsjwl.domain.Notice;
import cn.itcast.jsjwl.utils.PageModel;

public class NoticeService {

	public PageModel findNoticeWithPage(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
				NoticeDao NoticeDao = new NoticeDao();
				int totalRecords = NoticeDao.findTotalRecords();
				PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
				//2_为Page'Model设置集合（当前页的留言信息）
				List <Notice> list = NoticeDao.findNoticeWithPage(pm.getStartIndex(), pm.getPageSize());
				pm.setList(list);
				//3_为pagemodel设置url
				pm.setUrl("NoticeServlet?method=findNoticeWithPage");
				return pm;
	}

	public void addNotice(Notice notice) throws SQLException {
		NoticeDao NoticeDao=new NoticeDao();
		NoticeDao.addNotice(notice);
		
	}

	public void deleteNotice(String nId) throws SQLException {
		NoticeDao NoticeDao=new NoticeDao();
		NoticeDao.deleteNotice(nId);
	}

	public PageModel findNoticeWithPageByTeacher(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
		NoticeDao NoticeDao = new NoticeDao();
		int totalRecords = NoticeDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <Notice> list = NoticeDao.findNoticeWithPageByTeacher(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("NoticeServlet?method=findNoticeWithPageByTeacher");
		return pm;
	}

	public PageModel findNoticeWithPageByStu(int currentPageNum) throws SQLException {
		//创建PageModel对象，计算分页参数信息
		//1_创建pagemodel对象，计算分页参数信息
		NoticeDao NoticeDao = new NoticeDao();
		int totalRecords = NoticeDao.findTotalRecords();
		PageModel pm = new PageModel(currentPageNum, totalRecords, 5);
		//2_为Page'Model设置集合（当前页的留言信息）
		List <Notice> list = NoticeDao.findNoticeWithPageByStu(pm.getStartIndex(), pm.getPageSize());
		pm.setList(list);
		//3_为pagemodel设置url
		pm.setUrl("NoticeServlet?method=findNoticeWithPageByStu");
		return pm;
	}

	public Notice findNoticeByNidByStu(String nId) throws SQLException {
		//调用DAO层根据sID获取对象
		NoticeDao NoticeDao = new NoticeDao();
		return NoticeDao.findNoticeByNidByStu(nId);
	}

	public Notice findNoticeByNidByTeacher(String nId) throws SQLException {
		//调用DAO层根据sID获取对象
		NoticeDao NoticeDao = new NoticeDao();
		return NoticeDao.findNoticeByNidByTeacher(nId);
	}

}
