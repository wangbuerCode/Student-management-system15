package cn.itcast.jsjwl.service;

import java.sql.SQLException;
import java.util.List;

import cn.itcast.jsjwl.dao.StuDao;
import cn.itcast.jsjwl.domain.Student;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.utils.PageModel;

public class StuService {

	  public Student validateUserExist(String um) throws SQLException {
		// 调用DAO层功能
		StuDao stuDao = new StuDao();
		return stuDao.validateUserExist(um);
	}

	public Student stuLogin(String um, String up) throws SQLException {
		StuDao stuDao= new StuDao();
		return stuDao.stuLogin(um,up);
	}

	public int getOnlineNum(String classId) throws SQLException {
		StuDao stuDao=new StuDao();
		return stuDao.getOnlineNum(classId);
	}
	
	public Student getStudentByName(String userName) throws SQLException {
		StuDao stuDao = new StuDao();
		return stuDao.getStudentByName(userName);
	}

	public PageModel findOnlineStudentByClass(int currentNum, Teacher teacher) throws SQLException {
		  // 1创建PageModel对象，计算分页参数信息
		  int totalRecords=StuDao.findTotalRecords();
		  PageModel pm=new PageModel(currentNum,totalRecords,20);
		  //2为PageModel关联集合集合中存放的就是当前页的资料信息
		  //调用Dao层查看当前分页信息
		  StuDao StuDao=new StuDao();
		  List<Student>list=StuDao.findOnlineStudentByClass(pm.getStartIndex(),pm.getPageSize(),teacher);
		  pm.setList(list);
		  //3为PageModel关联Url属性
		  pm.setUrl("StuServlet?method=findOnlineStudentByClass");
		  return pm;
		  
		 }
	
	public PageModel findStudentsWithPage(int currentNum) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
		int totalRecords=StuDao.findTotalRecords();
		PageModel pm=new PageModel(currentNum,totalRecords,5);
		//2为PageModel关联集合集合中存放的就是当前页的资料信息
		//调用Dao层查看当前分页信息
		StuDao StuDao=new StuDao();
		List<Student>list=StuDao.findStudentsWithPage(pm.getStartIndex(),pm.getPageSize());
		pm.setList(list);
		//3为PageModel关联Url属性
		pm.setUrl("StuServlet?method=findStudentsWithPage");
		
		return pm;
	}

	public void addStudent(Student student) throws SQLException {
		StuDao StuDao=new StuDao();
		StuDao.addStudent(student);	
		
	}

	public void delStudentById(String sId) throws SQLException {
		StuDao StuDao=new StuDao();
		StuDao.delStudentById(sId);	
		
	}

	public void updateStudent(Student stu) throws SQLException {
			StuDao StuDao=new StuDao();
			StuDao.updateStudent(stu);
		}

	public PageModel findStudentByClass(int currentNum, Teacher teacher) throws SQLException {
		// 1创建PageModel对象，计算分页参数信息
		int totalRecords=StuDao.findTotalRecords();
		PageModel pm=new PageModel(currentNum,totalRecords,20);
		//2为PageModel关联集合集合中存放的就是当前页的资料信息
		//调用Dao层查看当前分页信息
		StuDao StuDao=new StuDao();
		List<Student>list=StuDao.findStudentByClass(pm.getStartIndex(),pm.getPageSize(),teacher);
		pm.setList(list);
		//3为PageModel关联Url属性
		pm.setUrl("StuServlet?method=findStudentByClass");
		return pm;
	}

	public int ifOnline(String um) throws SQLException {
		StuDao stuDao=new StuDao();
		return stuDao.ifOnline(um);
	}

	


}
