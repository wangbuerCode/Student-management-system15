package cn.itcast.jsjwl.service;

import java.sql.SQLException;

import cn.itcast.jsjwl.dao.AdminDao;
import cn.itcast.jsjwl.domain.Admin;


public class AdminService {

	public Admin adminLogin(String um, String up) throws SQLException {
		// 调用DAO层功能
		AdminDao AdminDao=new AdminDao();
		return AdminDao.adminLogin(um,up);
	}

	public void updateAdmin(Admin admin) throws SQLException {
		AdminDao AdminDao=new AdminDao();
		AdminDao.updateAdmin(admin);	
	}

	
}
