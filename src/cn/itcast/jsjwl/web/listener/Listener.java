package cn.itcast.jsjwl.web.listener;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import cn.itcast.jsjwl.domain.Admin;
import cn.itcast.jsjwl.domain.Student;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.service.AdminService;
import cn.itcast.jsjwl.service.StuService;
import cn.itcast.jsjwl.service.TeacherService;
import cn.itcast.jsjwl.utils.LongToDate;

public class Listener implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent event) {
	}
	@Override
		public void sessionDestroyed(HttpSessionEvent event) {
		      TeacherService TeacherService=new TeacherService();
		      Teacher teacher = (Teacher)event.getSession().getAttribute("teacher");
		      AdminService AdminService=new AdminService();
		      Admin admin = (Admin)event.getSession().getAttribute("admin");    
		      StuService stuService=new StuService();	
	          Student student=(Student)event.getSession().getAttribute("stu");	
		      if(teacher != null){
		       System.err.println("teacher -1");
		      }
		      else if(admin != null){
		       System.err.println("admin -1");
		      }
		      else if(student != null){
			  student.setOffline();
			  try {
			   stuService.updateStudent(student);
			  } catch (Exception e) {
			  }			  
			  System.out.println(student.getName() + " 退出");
			  int testNum = 0;
			  try {
			   testNum = stuService.getOnlineNum("1");
			  } catch (SQLException e) {
			   e.printStackTrace();
			  }
			  System.err.println("当前在线人数："+testNum);  
		      SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		      long b=event.getSession().getCreationTime();
		      Date date= new Date();
		      String time=sd.format(date);
		      long n=LongToDate.pare(time);
		      long diff=n-b;
		      long days = diff / (1000 * 60 * 60 * 24);  		      
		      long hours = (diff-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60);  
		      long minutes = (diff-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60); 
		      if(minutes>10) {
		    	  student.setOut();
		    	  try {
					   stuService.updateStudent(student);
					  } catch (Exception e) {
					  }			  
		      }
		      String outtime=LongToDate.longToDate(n);
		      System.err.println("销毁时间："+outtime);  
		      System.err.println("时长："+minutes);  
		      }
	       }	      
}

	
	

