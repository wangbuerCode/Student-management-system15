package cn.itcast.jsjwl.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


public class LoginFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {


    }


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		  HttpServletRequest req = (HttpServletRequest) request;
		  

		  String path = req.getRequestURI();

		  if(path.indexOf("/stu/") > -1) {
			  Object obj = req.getSession().getAttribute("stu"); 
			  if(null==obj) {
					//如果当前的session中没有登录成功的admin,转发到提示页面，请管理员登录之后再做操作
				  req.setAttribute("msg", "请学生登录之后再做操作");
				  req.getRequestDispatcher("/login.jsp").forward(req, response);
				  
			  }	else {
					chain.doFilter(request, response);
			   
			  }	System.err.println("1销毁时间：");  
		  }
		  else if(path.indexOf("/admin/") > -1) {
			  Object obj = req.getSession().getAttribute("admin"); 
			  if(null==obj) {
					//如果当前的session中没有登录成功的admin,转发到提示页面，请管理员登录之后再做操作
				  req.setAttribute("msg", "请管理员登录之后再做操作");
				  req.getRequestDispatcher("/jsjwl/login.jsp").forward(req, response);
				  
			  }	else {
					chain.doFilter(request, response);
			   
			  }	System.err.println("2销毁时间：");  
		  }
		  else if(path.indexOf("/atea/") > -1) {
			  Object obj = req.getSession().getAttribute("teacher"); 
			  if(null==obj) {
					//如果当前的session中没有登录成功的admin,转发到提示页面，请管理员登录之后再做操作
				  req.setAttribute("msg", "请教师登录之后再做操作");
				  req.getRequestDispatcher("/login.jsp").forward(req, response);
				 
			  }	else {
					chain.doFilter(request, response);
			   
			  }	System.err.println("3销毁时间：");  
		  }
		  else {
			  chain.doFilter(request, response);
			  System.err.println("4销毁时间：");  
		  }
	
}
	

	@Override
    public void destroy() {


    }

}
