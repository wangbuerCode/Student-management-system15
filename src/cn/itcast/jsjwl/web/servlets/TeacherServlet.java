package cn.itcast.jsjwl.web.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.service.TeacherService;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.web.base.BaseServlet;


public class TeacherServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	 
//登录
	public String teacherLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String um=request.getParameter("userName");
		String up=request.getParameter("userPw");
		TeacherService TeacherService=new TeacherService();
		Teacher teacher=TeacherService.teacherLogin(um,up);
		if(null==teacher) {
		request.setAttribute("msg", "账号密码有误");
		return "/login.jsp";
	}else {
        request.getSession().setAttribute("teacher", teacher); 
        response.sendRedirect("atea/index.jsp");
        return null;
       }
   }
	//findTeachersWithPage
	public String findTeachersWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受当前页
		int currentNum=Integer.parseInt(request.getParameter("num"));
		//调用业务层功能,返回PageModel
		TeacherService TeacherService=new TeacherService();
		PageModel pm=TeacherService.findTeachersWithPage(currentNum);
		//将PageModel放入request
		request.setAttribute("page", pm);
		//转发到/admin/tea/teaMana.jsp
		return"/admin/tea/teaMana.jsp";
	}
	//addTeacherUI
	public String addTeacherUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return"/admin/tea/teaAdd.jsp";	
	}
	//addTeacher
	public String addTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
		 //接收参数
		String teaid=request.getParameter("teaid");
		String name=request.getParameter("name");
		String sex=request.getParameter("sex");
		String tel=request.getParameter("tel");
		String class_id=request.getParameter("class_id");
		String job_title=request.getParameter("job_title");
		String faculty=request.getParameter("faculty");
		String login_pwd=request.getParameter("login_pwd");
		
		Teacher teacher=new Teacher();
		teacher.setTeaid(teaid);
		teacher.setName(name);
		teacher.setSex(sex);
		teacher.setTel(tel);
		teacher.setClass_id(class_id);
		teacher.setJob_title(job_title);
		teacher.setFaculty(faculty);
		teacher.setLogin_pwd(login_pwd);
		//调用业务层发布留言功能
		TeacherService TeacherService=new TeacherService();
		TeacherService.addTeacher(teacher);
		//重定向
		response.sendRedirect("/jsjwl/TeacherServlet?method=findTeachersWithPage&num=1");
		return null;
		
	}
	//delTeacherById
	public String delTeacherById(HttpServletRequest request, HttpServletResponse response) throws Exception {
		        //获取id
				String tId=request.getParameter("id");
				//调用业务层，删除
				TeacherService TeacherService=new TeacherService();
				TeacherService.delTeacherById(tId);
				//重定向
				response.sendRedirect("/jsjwl/TeacherServlet?method=findTeachersWithPage&num=1");
				return null;
	}
	//findTeachersWithPageByStu
	public String findTeachersWithPageByStu(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受当前页
		int currentNum=Integer.parseInt(request.getParameter("num"));
		//调用业务层功能,返回PageModel
		TeacherService TeacherService=new TeacherService();
		PageModel pm=TeacherService.findTeachersWithPageByStu(currentNum);
		//将PageModel放入request
		request.setAttribute("page", pm);
		//转发到/stu/teaMana.jsp
		return"/stu/teaInfo.jsp";
	}
	//findMyInfoUI
	public String findMyInfoUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return"/atea/userinfo/userinfo.jsp";	
	}
	//updateTeacher
	public String updateTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String teaid=request.getParameter("teaid");
		String name=request.getParameter("name");
		String sex=request.getParameter("sex");
		String class_id=request.getParameter("class_id");
		String tel=request.getParameter("tel");
		String login_pwd=request.getParameter("login_pwd");
		String faculty=request.getParameter("faculty");
		String job_title=request.getParameter("job_title");
		Teacher t=new Teacher();
		t.setTeaid(teaid);
		t.setName(name);
		t.setClass_id(class_id);
		t.setSex(sex);
		t.setTel(tel);
		t.setLogin_pwd(login_pwd);
		t.setFaculty(faculty);
		t.setJob_title(job_title);
		TeacherService TeacherService=new TeacherService();
		TeacherService.updateTeacher(t);
		return"/atea/userinfo/msg.jsp";
	}
	
	
}