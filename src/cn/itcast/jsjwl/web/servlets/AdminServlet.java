package cn.itcast.jsjwl.web.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Admin;
import cn.itcast.jsjwl.service.AdminService;
import cn.itcast.jsjwl.web.base.BaseServlet;


public class AdminServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
       

	//登录
	public String adminLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取账号和密码
		String um=request.getParameter("userName");
		String up=request.getParameter("userPw");
		//调用业务层登录功能返回admin对象
		AdminService AdminService=new AdminService();
		Admin admin=AdminService.adminLogin(um,up);
		//若admin为空
		if(null==admin) {
			//登录失败，向request放入提示信息
			request.setAttribute("msg", "账户或密码错误");
			return "/login.jsp";
		}else {
			//admin不为空
			//登录成功向session放入admin对象重定向到/admin/index.jsp
			request.getSession().setAttribute("admin", admin);
			response.sendRedirect("admin/index.jsp");
			return null;
		}
		
	}
    //findAdminInfoUI
	public String findAdminInfoUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return"/admin/userinfo/userPwd.jsp";		
	}
	//updateAdmin
	public String updateAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String rootid=request.getParameter("rootid");
		String rootname=request.getParameter("rootname");
		String rootpwd=request.getParameter("rootpwd");
		Admin admin=new Admin();
		admin.setRootid(rootid);
		admin.setRootname(rootname);
		admin.setRootpwd(rootpwd);
		AdminService AdminService=new AdminService();
		AdminService.updateAdmin(admin);
		return"/admin/userinfo/msg.jsp";		
	}
	
	
}

