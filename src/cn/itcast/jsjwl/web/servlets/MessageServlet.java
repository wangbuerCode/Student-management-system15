package cn.itcast.jsjwl.web.servlets;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Message;
import cn.itcast.jsjwl.domain.Student;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.service.MessageService;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.web.base.BaseServlet;

public class MessageServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	//查看当前学生的师生交流内容，分页
	public String findMessageWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取当前页
		int currentNum=Integer.parseInt(request.getParameter("num"));
		//获取session中的学生信息
		Student stu=(Student)(request.getSession().getAttribute("stu"));
		//将学生传递给业务层，调用业务层功能，返回PageModel
		MessageService messageService=new MessageService();
		PageModel pm=messageService.findMessageWithPage(currentNum,stu);
		//将PageModel放入request域对象内
		request.setAttribute("page",pm);
		//转发到messageAll.jsp	
		return "/stu/message/messageAll.jsp";
	}
	//addMessageUI
	public String addMessageUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    //仅仅是一个空跳转
		return "/stu/message/messageAdd.jsp";
	}
	//addMessage
	public String addMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    //接收参数
		//获取留言内容
		String content=request.getParameter("content");
		//获取session中的学生信息
		Student stu=(Student)(request.getSession().getAttribute("stu"));
		//创建message对象
		Message msg=new Message();
		msg.setContent(content);
		//获取当前时间.
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		msg.setLeveWordTime(sdf.format(new Date()));
		msg.setStuid(stu.getStuid());
		//调用业务层发布留言功能
		MessageService MessageService=new MessageService();
		MessageService.addMessage(msg);
		//转发到MessageServlet下的 findMessageWithPage?num=1
		response.sendRedirect("/jsjwl/MessageServlet?method=findMessageWithPage&num=1");
		return null;

	}
	//findMessagesWithPageByTeacher
		public String findMessagesWithPageByTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取当前页
			int currentPageNum = Integer.parseInt(request.getParameter("num"));
			//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
			MessageService MessageService = new MessageService();
			PageModel pm = MessageService.findMessagesWithPageByTeacher(currentPageNum);
			//将PageModel放入request对象
			request.setAttribute("page", pm);
			//转发到/atea/message/messageMana.jsp
			return "/atea/message/messageMana.jsp";
		}
		
		//replayUI
		public String replayUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//接收正在回复问题的编号
			String msgId = request.getParameter("id");
			//将编号放入request
			request.setAttribute("msgId", msgId);
			//转发到messageAdd.jsp
			return "/atea/message/messageAdd.jsp";
		}
		
		//replayMessage
		public String replayMessage(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//接收回复内容参数
			
			String content=request.getParameter("replay");
			//获取session中的学生信息
			Teacher teacher=(Teacher)(request.getSession().getAttribute("teacher"));
			String replay = request.getParameter("replay");
			//接收问题的id
			String id = request.getParameter("id");
			//回复老师姓名
			Message replaymsg=new Message();
			replaymsg.setReplay(content);
			//获取当前时间.
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			replaymsg.setReplayTime(sdf.format(new Date()));
			replaymsg.setReplayname(teacher.getName());
			//调用业务层回复内容
			MessageService MessageService = new MessageService();
			MessageService.replayMessage(replaymsg,id);
			//重定向
			response.sendRedirect("/jsjwl/MessageServlet?method=findMessagesWithPageByTeacher&num=1");
			return null;
		}
	//findMessageByMessageid
		public String findMessageByMessageid(HttpServletRequest request, HttpServletResponse response) throws Exception {
			  String messageId=request.getParameter("id");
		      MessageService MessageService=new MessageService();
		      Message message=MessageService.findMessageBymessageid(messageId);
		      request.setAttribute("message", message);
		      return "/stu/message/messageDetial.jsp";		
		}
	//findMessageWithPageByAdmin
		public String findMessageWithPageByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取当前页
			int currentPageNum = Integer.parseInt(request.getParameter("num"));
			//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
			MessageService MessageService = new MessageService();
			PageModel pm = MessageService.findMessageWithPageByAdmin(currentPageNum);
			//将PageModel放入request对象
			request.setAttribute("page", pm);
			//转发到/admin/message/messageMana.jsp
			return "/admin/message/messageMana.jsp";
		}
	//messageDel	
		public String messageDel(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取留言id
			String mId=request.getParameter("id");
			//调用业务层，删除
			MessageService MessageService=new MessageService();
			MessageService.messageDel(mId);
			//重定向
			response.sendRedirect("/jsjwl/MessageServlet?method=findMessageWithPageByAdmin&num=1");
			return null;
			
		}	
		
		
}	
