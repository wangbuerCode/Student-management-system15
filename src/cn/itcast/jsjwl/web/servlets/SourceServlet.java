package cn.itcast.jsjwl.web.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import cn.itcast.jsjwl.domain.Source;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.service.SourceService;
import cn.itcast.jsjwl.utils.DownLoadUtils;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.utils.UploadUtils;
import cn.itcast.jsjwl.web.base.BaseServlet;

public class SourceServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
       
    
	public String findPreSource(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//调用业务层，返回存储Source的对象的集合
		SourceService SourceService=new SourceService();
		List<Source> list=SourceService.findPreSource();
		//将集合放入request域对象内
		request.setAttribute("list", list);
		//转发到/stu/source/sourcePre.jsp
		return "/stu/source/sourcePre.jsp";
	}
     //findSourceWithPage
	public String findSourceWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //接受当前页参数
		int num=Integer.parseInt(request.getParameter("num"));
		//调用业务层，返回PageModel
		SourceService SourceService=new SourceService();
		PageModel pm=SourceService.findSourceWithPage(num);
		//由于当前功能是分页查询，除了查询资料信息，还需要获得当前分页信息		
		//如果仅仅是当前页资料信息，仅用一个集合即可，但分页信息比较复杂，所以用PageModle工具类
		//4_将PageModel对象放入request域对象内
		request.setAttribute("page", pm);
		//5_转发到/stu/source/sourceAll.jsp
		return"/stu/source/sourceAll.jsp"; 
	}
	//findSourceBySid
	public String findSourceBySid(HttpServletRequest request, HttpServletResponse response) throws Exception {
	      String sId=request.getParameter("id");
	      SourceService SourceService=new SourceService();
	      Source source=SourceService.findSourceBySid(sId);
	      request.setAttribute("source", source);
	      return "/stu/source/sourceDetial.jsp";
	}
	//downloadSource
	public String downloadSource(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接收文件id
		String id=request.getParameter("id");
		//调用service，根据id获取文件
		SourceService SourceService=new SourceService();
		Source source=SourceService.findSourceBySid(id);
		//获取项目下upload目录的绝对路径
		String realPath = getServletContext().getRealPath("/upload/");		
		//实例化一个doc对象，代表带下载的视频
		File file=new File(realPath,source.getPre_filename());
		response.setHeader("Content-disposition", "attachmen;sourcename="+source.getPre_filename());		
		
		DownLoadUtils.setConentType(request, source.getPre_filename(), response);
		//通过response设置一对消息头
		//通过doc获取输入流
		InputStream is=new FileInputStream(file);
		//通过respond获取输出流
		OutputStream os =response.getOutputStream();
		//将输入流的数据刷出到输出流中
	    IOUtils.copy(is,os);
	    IOUtils.closeQuietly(is);
	    IOUtils.closeQuietly(os); 	
	      return null;	
	}
	//findSourceWithPageByTeacher
	public String findSourceWithPageByTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受当前页参数
		int currentPageNum=Integer.parseInt(request.getParameter("num"));
		//调用业务层，返回PageModel
		SourceService SourceService=new SourceService();
		PageModel pm=SourceService.findSourceWithPageByTeacher(currentPageNum);
		//由于当前功能是分页查询，除了查询资料信息，还需要获得当前分页信息				
		//如果仅仅是当前页资料信息，仅用一个集合即可，但分页信息比较复杂，所以用PageModle工具
	    //4_将PageModel对象放入request域对象内
		request.setAttribute("page", pm);
		//5_转发到/stu/source/sourceMana.jsp
		return"/atea/source/sourceMana.jsp"; 	
	}
	//deleteSourceByTeacher
	public String deleteSourceByTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取资料id
		String sId=request.getParameter("id");
		//调用业务层，删除
		SourceService SourceService=new SourceService();
		SourceService.deleteSourceByTeacher(sId);
		//重定向
		response.sendRedirect("/jsjwl/SourceServlet?method=findSourceWithPageByTeacher&num=1");
		return null;
	}
	//addSourceUI
	public String addSourceUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return"/atea/source/sourceAdd.jsp";	
	}
	//addSource
	public String addSource(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受表单参数
				//String vedioName=request.getParameter("vedioName"); 
				//System.out.println(vedioName);//null
				
				//打印request对象内的输入流中的数据
				
				//InputStream is = request.getInputStream();
				//int i=is.read();
				//while(i!=-1) {
					//System.out.print((char)i);
					//i=is.read();
				//}
		  
		
		//获取session中的学生信息
		Map<String,String> map=new HashMap<String,String>();
		Source source=new Source();
		//1_创建DiskFiletemFactory对象,设置允许上传文件大小
		DiskFileItemFactory fac=new DiskFileItemFactory();
		fac.setSizeThreshold(1024*1024*200);//允许上传文件最大为200mb
		//2_创建ServletFileUpload upload
		ServletFileUpload upload=new ServletFileUpload(fac);
		
		//3_通过upload解析request,得到集合<FileItem>,FileItem代表什么？工具就将请求体中每对分割线中间的内容封装为一个FileItem对象
		List<FileItem> list=upload.parseRequest(request);
		//4_遍历集合
		for (FileItem item :list) {
			//item.isFormField()判断当前的item是否表单项目
			//5_判断当前FileItem是普通项还是上传项？
			//什么是普通项：表单中的普通字段，非上传字段
			//什么是上传项：表单中包含file组件上传项，携带着上传到服务端文件
			//如果是普通项：获取到对应的表单名称和表单内容 
	      
			if(item.isFormField()) {
				//上传项

				String name=item.getFieldName();
				String vaule=item.getString();
				//System.out.println(name);
				//System.out.println(vaule);
				map.put(item.getFieldName(), item.getString("utf-8"));
			}else {
				  // 如果是上传项：在服务端指定目录/upload/ 创建一个文件，将上传项中文件的二进制数据输出到创建好的文件中   
				//获取文件名称
				String fName=item.getName();
				//System.out.println("文件名称"+fName);
				//获取服务端upload真实路径
				String realPath=getServletContext().getRealPath("/upload/");
				//D:/tomcat/tomcat/webapps/jsjwl/upload
				String uuidName=UploadUtils.getUUIDName(fName);
				//XXXXXX.mp4
				//在服务端指定路径下创建文件
				File f=new File(realPath,uuidName);
				if(!f.exists()) {
					f.createNewFile();
					//创建文件此时其中没有内容
				}
				item.write(f);//将上传到服务端的文件中的二进制数据输出到文件中
				map.put("now_filename", uuidName);
				map.put("pre_filename", fName);
			}
		}
		        //将MAP中的数据封装在Source对象上
			BeanUtils.populate(source, map);
			System.out.println(source);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			source.setUptime(sdf.format(new Date()));
			Teacher teacher=(Teacher)(request.getSession().getAttribute("teacher"));
			source.setUname(teacher.getName());
			source.setDel("no");		
		//6_将普通项的数据以及文件的位置传递到service,dao.进行数据的保存
            SourceService SourceService=new SourceService();
            SourceService.addSource(source);
		response.sendRedirect("/jsjwl/SourceServlet?method=findSourceWithPageByTeacher&num=1");
		return null;
	}
	//deleteSourceByAdmin
	public String deleteSourceByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取资料id
		String sId=request.getParameter("id");
		//调用业务层，删除
		SourceService SourceService=new SourceService();
		SourceService.deleteSourceByAdmin(sId);
		//重定向
		response.sendRedirect("/jsjwl/SourceServlet?method=findsourceWithPageByAdmin&num=1");
		return null;
	}
	//findsourceWithPageByAdmin
	public String findsourceWithPageByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //接受当前页参数
		int num=Integer.parseInt(request.getParameter("num"));
		//调用业务层，返回PageModel
		SourceService SourceService=new SourceService();
		PageModel pm=SourceService.findsourceWithPageByAdmin(num);
		//由于当前功能是分页查询，除了查询资料信息，还需要获得当前分页信息		
		//如果仅仅是当前页资料信息，仅用一个集合即可，但分页信息比较复杂，所以用PageModle工具类
		//4_将PageModel对象放入request域对象内
		request.setAttribute("page", pm);
		//5_转发到/admin/source/sourceMana.jsp
		return"/admin/source/sourceMana.jsp"; 
	}
}
		

	
