package cn.itcast.jsjwl.web.servlets;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.domain.TeachingNotice;
import cn.itcast.jsjwl.service.TeachingNoticeService;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.web.base.BaseServlet;


public class TeachingNoticeServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	public String findTeachingNoticeWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取当前页
		int currentPageNum = Integer.parseInt(request.getParameter("num"));
		//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
		TeachingNoticeService TeachingNoticeService = new TeachingNoticeService();
		PageModel pm = TeachingNoticeService.findTeachingNoticeWithPage(currentPageNum);
		//将PageModel放入request对象
		request.setAttribute("page", pm);
		//转发到/admin/message/messageMana.jsp
		return "/atea/teachingNotice/teachingNoticeMana.jsp";
	}
	//addNoticeUI
	public String addTeachingNoticeUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    //仅仅是一个空跳转
		return "/atea/teachingNotice/teachingNoticeAdd.jsp";
	}
	//addTeachingNotice
	public String addTeachingNotice(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    //接收参数
		//获取留言内容
		String time=request.getParameter("time");
		String address=request.getParameter("address");
		String remark=request.getParameter("remark");
		//获取session中的学生信息
		Teacher teacher=(Teacher)(request.getSession().getAttribute("teacher"));
		//创建message对象
		TeachingNotice teachingNotice=new TeachingNotice();
		teachingNotice.setTime(time);
		teachingNotice.setAddress(address);
		teachingNotice.setRemark(remark);
		//获取当前时间.
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		teachingNotice.setUptime(sdf.format(new Date()));
		teachingNotice.setUname(teacher.getName());
		//调用业务层发布留言功能
		TeachingNoticeService TeachingNoticeService=new TeachingNoticeService();
		TeachingNoticeService.addTeachingNotice(teachingNotice);
		//转发到MessageServlet下的 findMessageWithPage?num=1
		response.sendRedirect("/jsjwl/TeachingNoticeServlet?method=findTeachingNoticeWithPage&num=1");
		return null;

	}
	//findTeachingNoticeWithPageByAdmin
	public String findTeachingNoticeWithPageByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取当前页
		int currentPageNum = Integer.parseInt(request.getParameter("num"));
		//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
		TeachingNoticeService TeachingNoticeService = new TeachingNoticeService();
		PageModel pm = TeachingNoticeService.findTeachingNoticeWithPageByAdmin(currentPageNum);
		//将PageModel放入request对象
		request.setAttribute("page", pm);
		//转发到/admin/message/messageMana.jsp
		return "/admin/teachingNotice/teachingNoticeMana.jsp";
	}
	//deleteTeachingNoticeByAdmin
	public String deleteTeachingNoticeByAdmin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取留言id
		String tnId=request.getParameter("id");
		//调用业务层，删除
		TeachingNoticeService TeachingNoticeService=new TeachingNoticeService();
		TeachingNoticeService.deleteTeachingNoticeByAdmin(tnId);
		//重定向
		response.sendRedirect("/jsjwl/TeachingNoticeServlet?method=deleteTeachingNoticeByAdmin&num=1");
		return null;
		
	}
	//findTeachingNoticeByStu
	public String findTeachingNoticeByStu(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取当前页
		int currentPageNum = Integer.parseInt(request.getParameter("num"));
		//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
		TeachingNoticeService TeachingNoticeService = new TeachingNoticeService();
		PageModel pm = TeachingNoticeService.findTeachingNoticeByStu(currentPageNum);
		//将PageModel放入request对象
		request.setAttribute("page", pm);
		//转发到/admin/message/messageMana.jsp
		return "/stu/teachingNotice/teachingNoticeAll.jsp";
	}
	//findTeachingNoticeById
	public String findTeachingNoticeById(HttpServletRequest request, HttpServletResponse response) throws Exception {
	      String tId=request.getParameter("id");
	      TeachingNoticeService TeachingNoticeService = new TeachingNoticeService();
	      TeachingNotice teachingNotice=TeachingNoticeService.findTeachingNoticeById(tId);
	      request.setAttribute("teachingNotice", teachingNotice);
	      return "/stu/teachingNotice/teachingNoticeDetial.jsp";
	}
}
