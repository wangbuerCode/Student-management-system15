package cn.itcast.jsjwl.web.servlets;

import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Student;
import cn.itcast.jsjwl.domain.Teacher;
import cn.itcast.jsjwl.service.StuService;
import cn.itcast.jsjwl.utils.LongToDate;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.web.base.BaseServlet;


public class StuServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
  //校验用户名是否存在

	public String stuLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取账号和密码
		String um=request.getParameter("userName");
		String up=request.getParameter("userPw");
		//调用业务层登录功能返回stu对象
		StuService stuService=new StuService();
		Student student=stuService.stuLogin(um,up);
	
		//若stu为空
		if(null==student) {
			//登录失败，向request放入提示信息
			request.setAttribute("msg", "账户或密码错误");
			return "/login.jsp";
		}else {
			//Stu不为空
			int a=stuService.ifOnline(um);
			if(a==1) {
				//用户已登录
				request.setAttribute("msg", "用户已登录！请勿重复登录");
				return "/login.jsp";
			}else {
			
				//登录成功向session放入Stu对象重定向到/site/index.jsp
			    student.setOnline();
			    stuService.updateStudent(student);
			    request.getSession().setAttribute("stu", student);
			    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        long b=request.getSession().getCreationTime();
		        String intime=LongToDate.longToDate(b);
			    response.sendRedirect("/jsjwl/stu/index.jsp");
			    //测试代码
		    	System.out.println(um + " 登陆");
			    System.out.println(intime + " 创建时间");
	 		    int testNum = stuService.getOnlineNum("1");
	 		    System.err.println("当前在线人数："+testNum);			
			    return null;
			}
		}
	}

	//用户退出
	public String stuLogout(HttpServletRequest request, HttpServletResponse response) throws Exception {

			
		Student student = (Student)request.getSession().getAttribute("stu");		
		
		//获取session，使其失效	
		request.getSession().invalidate();
		response.sendRedirect("/jsjwl/login.jsp");		
		//测试代码
	 
		return null;
	}
	
	public int getOnlineNum(String classId) throws SQLException {
		StuService stuService=new StuService();
		return stuService.getOnlineNum(classId);
	}

	//findOnlineStudentByClass
	  public String findOnlineStudentByClass(HttpServletRequest request, HttpServletResponse response) throws Exception {
	  //接受当前页
	  int currentNum=Integer.parseInt(request.getParameter("num"));
	  Teacher teacher=(Teacher)(request.getSession().getAttribute("teacher"));
	  //调用业务层功能,返回PageModel
	  StuService StuService=new StuService();
	  PageModel pm=StuService.findOnlineStudentByClass(currentNum,teacher);
	  //将PageModel放入request
	  request.setAttribute("page", pm);
	  //转发到/atea/stu/stuMana.jsp
	  return"/atea/stu/stuOnline.jsp";
	 }
	  
	//findStudentsWithPage	
	public String findStudentsWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受当前页
		int currentNum=Integer.parseInt(request.getParameter("num"));
		//调用业务层功能,返回PageModel
		StuService StuService=new StuService();
		PageModel pm=StuService.findStudentsWithPage(currentNum);
		//将PageModel放入request
		request.setAttribute("page", pm);
		//转发到/admin/stu/stuMana.jsp
		return"/admin/stu/stuMana.jsp";
	}
    //addStudentUI
	public String addStudentUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
			return"/admin/stu/stuAdd.jsp";	
	}	
	//addStudent
	public String addStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
			 //接收参数
			String stuid=request.getParameter("stuid");
			String name=request.getParameter("name");
			String class_id=request.getParameter("class_id");
			String sex=request.getParameter("sex");
			String tel=request.getParameter("tel");
			String faculty=request.getParameter("faculty");
			String loginpwd=request.getParameter("loginpwd");
			int testscore = Integer.parseInt(request.getParameter("testscore"));
			int expscore=Integer.parseInt(request.getParameter("expscore"));
			int videoscore=Integer.parseInt(request.getParameter("videoscore"));
			int sumscore =Integer.parseInt(request.getParameter("sumscore"));
			
			Student student=new Student();
			student.setStuid(stuid);
			student.setName(name);
			student.setClass_id(class_id);
			student.setSex(sex);
			student.setTel(tel);
			student.setFaculty(faculty);
			student.setLoginpwd(loginpwd);
			student.setTestscore(testscore);
			student.setExpscore(expscore);
			student.setVideoscore(videoscore);
			student.setSumscore(sumscore);
			//调用业务层发布留言功能
			StuService StuService=new StuService();
			StuService.addStudent(student);
			//重定向
			response.sendRedirect("/jsjwl/StuServlet?method=findStudentsWithPage&num=1");
			return null;
			
		}	
	//delStudentById
     public String delStudentById(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	 //获取id
			String sId=request.getParameter("id");
			//调用业务层，删除
			StuService StuService=new StuService();
			StuService.delStudentById(sId);
			//重定向
			response.sendRedirect("/jsjwl/StuServlet?method=findStudentsWithPage&num=1");
			return null;        
		}
	//findStuInfoUI
     public String findStuInfoUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	 return"/stu/userinfo/userinfo.jsp";
    	 
     }
   //updateStudent
 	public String updateStudent(HttpServletRequest request, HttpServletResponse response) throws Exception {
 		String stuid=request.getParameter("stuid");
 		String name=request.getParameter("name");
 		String sex=request.getParameter("sex");
 		String class_id=request.getParameter("class_id");
 		String tel=request.getParameter("tel");
 		String loginpwd =request.getParameter("loginpwd");
 		String faculty=request.getParameter("faculty");
 		Student stu=new Student();
 		stu.setStuid(stuid);
 		stu.setName(name);
 		stu.setClass_id(class_id);
 		stu.setSex(sex);
 		stu.setTel(tel);
 		stu.setLoginpwd(loginpwd);
 		stu.setFaculty(faculty);
 		StuService StuService=new StuService();
 		StuService.updateStudent(stu);		
 		return"/stu/userinfo/msg.jsp";
 	}
 	//findStudentByClass
 	public String findStudentByClass(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//接受当前页
		int currentNum=Integer.parseInt(request.getParameter("num"));
		Teacher teacher=(Teacher)(request.getSession().getAttribute("teacher"));
		//调用业务层功能,返回PageModel
		StuService StuService=new StuService();
		PageModel pm=StuService.findStudentByClass(currentNum,teacher);
		//将PageModel放入request
		request.setAttribute("page", pm);
		//转发到/admin/stu/stuMana.jsp
		return"/atea/stu/stuInfo.jsp";
	}


 	
 	
 	
}
