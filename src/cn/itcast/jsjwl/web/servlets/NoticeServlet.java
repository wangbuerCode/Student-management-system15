package cn.itcast.jsjwl.web.servlets;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.jsjwl.domain.Admin;
import cn.itcast.jsjwl.domain.Notice;
import cn.itcast.jsjwl.service.NoticeService;
import cn.itcast.jsjwl.utils.PageModel;
import cn.itcast.jsjwl.web.base.BaseServlet;

public class NoticeServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
      //findNoticeWithPage
	public String findNoticeWithPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//获取当前页
		int currentPageNum = Integer.parseInt(request.getParameter("num"));
		//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
		NoticeService NoticeService = new NoticeService();
		PageModel pm = NoticeService.findNoticeWithPage(currentPageNum);
		//将PageModel放入request对象
		request.setAttribute("page", pm);
		//转发到/admin/message/messageMana.jsp
		return "/admin/notice/noticeMana.jsp";
	}
	//addNoticeUI
		public String addNoticeUI(HttpServletRequest request, HttpServletResponse response) throws Exception {
		    //仅仅是一个空跳转
			return "/admin/notice/noticeAdd.jsp";
		}
		//addNotice
		public String addNotice(HttpServletRequest request, HttpServletResponse response) throws Exception {
		    //接收参数
			//获取留言内容
			String title=request.getParameter("title");
			String content=request.getParameter("content");
			//获取session中的学生信息
			Admin admin=(Admin)(request.getSession().getAttribute("admin"));
			//创建message对象
			Notice notice=new Notice();
			notice.setContent(content);
			notice.setTitle(title);
			//获取当前时间.
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			notice.setTime(sdf.format(new Date()));
			notice.setUpname(admin.getRootname());
			//调用业务层发布留言功能
			NoticeService NoticeService=new NoticeService();
			NoticeService.addNotice(notice);
			//转发到MessageServlet下的 findMessageWithPage?num=1
			response.sendRedirect("/jsjwl/NoticeServlet?method=findNoticeWithPage&num=1");
			return null;

		}
		//deleteNotice
		public String deleteNotice(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取留言id
			String nId=request.getParameter("id");
			//调用业务层，删除
			NoticeService NoticeService=new NoticeService();
			NoticeService.deleteNotice(nId);
			//重定向
			response.sendRedirect("/jsjwl/NoticeServlet?method=findNoticeWithPage&num=1");
			return null;
			
		}	
		//findNoticeWithPageByTeacher
		public String findNoticeWithPageByTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取当前页
			int currentPageNum = Integer.parseInt(request.getParameter("num"));
			//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
			NoticeService NoticeService = new NoticeService();
			PageModel pm = NoticeService.findNoticeWithPageByTeacher(currentPageNum);
			//将PageModel放入request对象
			request.setAttribute("page", pm);
			//转发到/admin/message/messageMana.jsp
			return "/atea/notice/notice.jsp";
			
		}
		//findNoticeWithPageByStu
		public String findNoticeWithPageByStu(HttpServletRequest request, HttpServletResponse response) throws Exception {
			//获取当前页
			int currentPageNum = Integer.parseInt(request.getParameter("num"));
			//调用业务层功能，返回PageModel对象（1_第一页留言信息2_分页参数信息3_URl）
			NoticeService NoticeService = new NoticeService();
			PageModel pm = NoticeService.findNoticeWithPageByStu(currentPageNum);
			//将PageModel放入request对象
			request.setAttribute("page", pm);
			//转发到/admin/message/messageMana.jsp
			return "/stu/notice/notice.jsp";			
		}
		//findNoticeByNidByStu
		public String findNoticeByNidByStu(HttpServletRequest request, HttpServletResponse response) throws Exception {
			 String nId=request.getParameter("id");
		      NoticeService NoticeService=new NoticeService();
		      Notice notice=NoticeService.findNoticeByNidByStu(nId);
		      request.setAttribute("notice", notice);
		      return "/stu/notice/noticeDetial.jsp";
		}
		//findNoticeByNidByTeacher
		public String findNoticeByNidByTeacher(HttpServletRequest request, HttpServletResponse response) throws Exception {
			 String nId=request.getParameter("id");
		      NoticeService NoticeService=new NoticeService();
		      Notice notice=NoticeService.findNoticeByNidByTeacher(nId);
		      request.setAttribute("notice", notice);
			  return "/atea/notice/noticeDetial.jsp";			
		}
}
