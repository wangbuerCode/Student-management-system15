package cn.itcast.jsjwl.utils;

import java.util.List;

//工具类
//携带数据1当前页资料信息2分页参数信息
public class PageModel {
    //1  当前页资料信息
	private List list;


	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
	//2 分页参数
	private int currentPageNum;//当前页，客户端浏览器传到服务端
	private int pageSize;//每页条数,约定好的
	private int totalRecords;//总共记录条数，仓库中获取
	private int totalPageNum;//共几页
	private int prePageNum;//前一页
	private int nextPageNum;//后一页
	private int startIndex;//初始索引值，本质上是分页语句中limit后的第一个数值
	//拓展页
	private int startPage;
	private int endPage;
	//完善属性
	private String url;

    public PageModel(int currentPageNum,int totalRecords,int pageSize) {
		this.currentPageNum=currentPageNum;
		this.totalRecords=totalRecords;
		this.pageSize=pageSize;
		
		totalPageNum= (totalRecords%pageSize==0)?(totalRecords/pageSize):(totalRecords/pageSize+1);
		startIndex=(currentPageNum-1)*pageSize;
		
		
		prePageNum=currentPageNum-1;
		if(currentPageNum<=1) {
			prePageNum=1;
		}
		nextPageNum=currentPageNum+1;
		if(currentPageNum==totalPageNum) {
			nextPageNum=totalPageNum;
		}
		
		
		startPage=currentPageNum-4;
		endPage=currentPageNum+4;
		if(totalPageNum<=9){
			//总共不到9页，所有的页数都要显示
			startPage=1;
			endPage=totalPageNum;
		}else {
			if(startPage<1){
				startPage=1;
				endPage=startPage+8;
			}
			if(endPage>totalPageNum) {
				endPage=totalPageNum;
				startPage=totalPageNum-8;
			}
		}
	
	}
	
	public int getCurrentPageNum() {
		return currentPageNum;
	}

	public void setCurrentPageNum(int currentPageNum) {
		this.currentPageNum = currentPageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPageNum() {
		return totalPageNum;
	}

	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public int getPrePageNum() {
		return prePageNum;
	}

	public void setPrePageNum(int prePageNum) {
		this.prePageNum = prePageNum;
	}

	public int getNextPageNum() {
		return nextPageNum;
	}

	public void setNextPageNum(int nextPageNum) {
		this.nextPageNum = nextPageNum;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
