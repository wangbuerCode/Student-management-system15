package cn.itcast.jsjwl.domain;

public class Message {
    private int messageid;
    private String content;
    private String stuid;
    private String leveWordTime;
    private String replay ;
    private String replayname;
    
	private String replayTime;
    
    public int getMessageid() {
		return messageid;
	}
	public void setMessageid(int messageid) {
		this.messageid = messageid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getLeveWordTime() {
		return leveWordTime;
	}
	public void setLeveWordTime(String leveWordTime) {
		this.leveWordTime = leveWordTime;
	}
	public String getReplay() {
		return replay;
	}

	public void setReplay(String replay) {
		this.replay = replay;
	}
	public String getReplayTime() {
		return replayTime;
	}
	public void setReplayTime(String replayTime) {
		this.replayTime = replayTime;
	}
	public String getReplayname() {
		return replayname;
	}
	public void setReplayname(String replayname) {
		this.replayname = replayname;
	}
    
}
