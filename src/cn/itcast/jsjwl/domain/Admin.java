package cn.itcast.jsjwl.domain;

public class Admin {
     private int id;
     private String rootid;
     private String rootname;
     private String rootpwd;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRootid() {
		return rootid;
	}
	public void setRootid(String rootid) {
		this.rootid = rootid;
	}
	public String getRootname() {
		return rootname;
	}
	public void setRootname(String rootname) {
		this.rootname = rootname;
	}
	public String getRootpwd() {
		return rootpwd;
	}
	public void setRootpwd(String rootpwd) {
		this.rootpwd = rootpwd;
	}	
}
