package cn.itcast.jsjwl.domain;

public class Source {
     private int _file_id ;
     private String filename;
     private String uname;
     private String pre_filename;
     private String now_filename;
     private String uptime;
     private String file_type;
     private String del;
	public int get_file_id() {
		return _file_id;
	}
	public void set_file_id(int _file_id) {
		this._file_id = _file_id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPre_filename() {
		return pre_filename;
	}
	public void setPre_filename(String pre_filename) {
		this.pre_filename = pre_filename;
	}
	public String getNow_filename() {
		return now_filename;
	}
	public void setNow_filename(String now_filename) {
		this.now_filename = now_filename;
	}
	public String getUptime() {
		return uptime;
	}
	public void setUptime(String uptime) {
		this.uptime = uptime;
	}
	public String getFile_type() {
		return file_type;
	}
	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	@Override
	public String toString() {
		return "Source [_file_id=" + _file_id + ", filename=" + filename + ", uname=" + uname + ", pre_filename="
				+ pre_filename + ", now_filename=" + now_filename + ", uptime=" + uptime + ", file_type=" + file_type
				+ ", del=" + del + "]";
	}
	
}
