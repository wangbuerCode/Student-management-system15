package cn.itcast.jsjwl.domain;

public class Student {
    private int id;
    private String stuid;
    private String name;
    private String class_id;
    private String sex;
    private String tel;
    private String faculty;
    private String loginpwd;
    private int activity;
    private int online;
    private int testscore;
    private int expscore;
    private int videoscore;
    private int sumscore;
//    private long pre_timestamp;
//    private long timestamp;
    private String del;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClass_id() {
		return class_id;
	}
	public void setClass_id(String class_id) {
		this.class_id = class_id;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getFaculty() {
		return faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	public String getLoginpwd() {
		return loginpwd;
	}
	public void setLoginpwd(String loginpwd) {
		this.loginpwd = loginpwd;
	}
	public int getActivity() {
		return activity;
	}
	public void setOut() {
		this.activity = activity+1;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	public int getOnline() {
		return this.online;
	}
	
	public void setOffline() {
		this.online=0;
	}
	public void setOnline() {
		this.online = 1;
//		this.timestamp=System.currentTimeMillis();
	}
	public int getTestscore() {
		return testscore;
	}
	public void setTestscore(int testscore) {
		this.testscore = testscore;
	}
	public int getExpscore() {
		return expscore;
	}
	public void setExpscore(int expscore) {
		this.expscore = expscore;
	}
	public int getVideoscore() {
		return videoscore;
	}
	public void setVideoscore(int videoscore) {
		this.videoscore = videoscore;
	}
	public int getSumscore() {
		return sumscore;
	}
	public void setSumscore(int sumscore) {
		this.sumscore = sumscore;
	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
//	public void setPreTimeStamp(int times) {
//		this.pre_timestamp=times;
//	}
//	public long getPreTimeStamp() {
//		return this.pre_timestamp;
//	}
//	public void setTimeStamp(long time) {
//		this.timestamp=time;
//	}
//	public long getTimeStamp() {
//		return this.timestamp;
//	}
}
