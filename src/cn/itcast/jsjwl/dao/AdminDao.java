package cn.itcast.jsjwl.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import cn.itcast.jsjwl.domain.Admin;
import cn.itcast.jsjwl.utils.JDBCUtils;

public class AdminDao {

	public Admin adminLogin(String um, String up) throws SQLException {
		String sql="SELECT *  FROM root WHERE rootid=? AND rootpwd=?";
		QueryRunner qr=new QueryRunner(JDBCUtils.getDataSource());
        return qr.query(sql, new BeanHandler<Admin>(Admin.class),um,up);
	}

	public void updateAdmin(Admin admin) throws SQLException {
		String sql="UPDAT root SET rootname=？，rootpwd=? WHERE rootid=?";
		Object[] params= {admin.getRootname(),admin.getRootpwd(),admin.getRootid()} ;
		QueryRunner qr=new QueryRunner(JDBCUtils.getDataSource());
		qr.update(sql,params);
	}

	

}
