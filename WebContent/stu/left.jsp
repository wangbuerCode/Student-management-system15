<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript">


function logout(){
	if(confirm("确认退出吗？")){
		location.href="/jsjwl/StuServlet?method=stuLogout";
	}
}

      
</script>
    </head>
    <body>
	<div id="logo">
	      <br/><br/><br/><span style="font-size: 25px;margin-left: 5px;">学生信息系统</span>
	</div>
	
    <div id="nav">
      <ul>
        <li><a href="${pageContext.request.contextPath}/StuServlet?method=findStuInfoUI" style="font-family: 微软雅黑;font-size: 12px;">个人信息</a></li>
        <li><a href="${pageContext.request.contextPath}/TeacherServlet?method=findTeachersWithPageByStu&num=1" style="font-family: 微软雅黑;font-size: 12px;">老师信息</a></li>
        <li><a href="${pageContext.request.contextPath}/SourceServlet?method=findPreSource" style="font-family: 微软雅黑;font-size: 12px;">教学资料</a></li>
        <li><a href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=findTeachingNoticeByStu&num=1" style="font-family: 微软雅黑;font-size: 12px;">授课安排</a></li>
        <li><a href="${pageContext.request.contextPath}/NoticeServlet?method=findNoticeWithPageByStu&num=1"style="font-family: 微软雅黑;font-size: 12px;">查看通知</a></li>
		<li><a href="${pageContext.request.contextPath}/MessageServlet?method=findMessageWithPage&num=1" style="font-family: 微软雅黑;font-size: 12px;">师生交流</a></li>
<%--		<li><a href="/jsjwl/404.jsp"  target="1" style="font-family: 微软雅黑;font-size: 12px;">在线测试系统</a></li>--%>
<%--		<li><a href="/jsjwl/404.jsp"  target="1" style="font-family: 微软雅黑;font-size: 12px;">视频学习系统</a></li>--%>
<%--	    <li><a href="/jsjwl/404.jsp"  target="1" style="font-family: 微软雅黑;font-size: 12px;">实验及课程设计系统</a></li>--%>
<%--		<li><a href="/jsjwl/404.jsp"  target="1" style="font-family: 微软雅黑;font-size: 12px;">过程性评价系统</a></li>	--%>
		
      </ul>
    </div>
    
    <div id="news">
       <TABLE border="0" cellSpacing="3" cellPadding="3" width="178" height="22">
			 <TR>
			     <TD height="22" vAlign="middle" width="100%">
	
						        <br/>
						                        欢迎您：${stu.name} &nbsp;&nbsp;<br>
							    <a href="#" onclick="logout()">安全退出</a> 
							    <img id="indicator1" src="/jsjwl/images/loading.gif" style="display:none"/>
							    <br/><br/><br/>
				
			      </TD>           
			 </TR>
		</TABLE>
       <!-- login__end -->
       
      <div class="hr-dots"> </div>
      <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </div>
    
  	</body>
</html>