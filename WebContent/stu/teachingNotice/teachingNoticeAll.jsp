<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<link rel="stylesheet" href="/jsjwl/css/qiantai.css" type="text/css" charset="utf-8" />
	
	<style type="text/css">
		.c1-bline{border-bottom:#999 1px dashed;border-top:1px;}
		.f-right{float:right}
		.f-left{float:left}
		.clear{clear:both}
    </style>
	
	
  </head>
  
  <body>
  <div id="wrapper">
      
      <div id="header"></div>
      
      <!-- left__start -->
      <div id="left">
	       <jsp:include flush="true" page="/stu/left.jsp"></jsp:include>
      </div>
      <!-- left__end -->
      
      

	<div id="right">
      	  <!-- 111 -->
      	  <h2>授课安排</h2>
	      <div id="welcome">
	        <div>
	           <div class="c1-body">
			   <table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="14" background="/jsjwl/img/tbg.gif">&nbsp;&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
					<td width="4%">序号</td>
					<td width="15%">上课时间</td>
					<td width="12%">上课地点</td>
					<td width="6%">发布者</td>
					<td width="8%">发布时间</td>
		        </tr>
				
			 <c:forEach items="${page.list}" var="tn"  varStatus="status">
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						${status.index+1}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.time}
					</td>					
				
					<td bgcolor="#FFFFFF" align="center">
						${tn.address}
						&nbsp;&nbsp;&nbsp;
						  <a href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=findTeachingNoticeById&id=${tn.id}">详情</a></td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.uname}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.uptime}
					</td>
				</tr>
			  </c:forEach>
			 </tr>
			</table>
		     
		           
	        <%@include file="/jsp/pageFile.jsp" %>
  					<div class="pg-3"></div>		  
     		   </div>
	        </div>
	        
	        
	      </div>
	      <!-- 111 -->
      </div>
  
      <div class="clear"> </div>
      
      
      <div id="footer">
	      <div id="copyright">
	        Copyright &copy;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	      </div>
	      <div id="footerline"></div>
      </div>
  </div>
</body>

</html>




