<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<link rel="stylesheet" href="/jsjwl/css/qiantai.css" type="text/css" charset="utf-8" />
	
	<style type="text/css">
		.c1-bline{border-bottom:#999 1px dashed;border-top:1px;}
		.f-right{float:right}
		.f-left{float:left}
		.clear{clear:both}
    </style>
	
	<script type="text/javascript" src="/jsjwl/js/public.js"></script>
  </head>
  
  <body>
  <div id="wrapper">
      <div id="header"></div>
      <div id="left">
	      <jsp:include flush="true" page="/stu/left.jsp"></jsp:include>
      </div>
      <div id="right">
      	  <!-- 111 -->
      	  <h2>授课安排</h2>
	      <div id="welcome">
	        <div>
	           <div class="c1-body">
                   <table width="100%" border="0">
					   
					    <tr>
					        <td align="center">上课时间：${teachingNotice.time}<hr/></td>
					    </tr>
					    <tr>
					        <td align="center">上课地点：${teachingNotice.address}<hr/></td>
					    </tr>
					    <tr>
					       <td align="center">备注:${teachingNotice.remark}<hr/></td>
					    </tr>
					    <tr>
					       <td align="center">发布者:${teachingNotice.uname}<hr/></td>
					    </tr>
					    <tr>
					       <td align="center">发布时间:${teachingNotice.uptime}<hr/></td>
					    </tr>
					</table>
		       </div>
	        </div>
	         <p class="more"><a href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=findTeachingNoticeByStu&num=1">返回</a></p> 
	      </div>
	      <!-- 111 -->
      </div>
      <div class="clear"> </div>
      <div id="footer">
	      <div id="copyright">
	        Copyright &copy; 2020 Company Name All right reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	      </div>
	      <div id="footerline"></div>
      </div>
  </div>
</body>
</html>
