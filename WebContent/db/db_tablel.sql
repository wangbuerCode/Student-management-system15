/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.27 : Database - db_table
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_table` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_table`;

/*Table structure for table `message` */

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(2000) NOT NULL,
  `stuid` varchar(25) NOT NULL,
  `leveWordTime` varchar(50) NOT NULL,
  `replay` varchar(2000) DEFAULT NULL,
  `replayname` varchar(50) DEFAULT NULL,
  `replayTime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`messageid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `message` */

insert  into `message`(`messageid`,`content`,`stuid`,`leveWordTime`,`replay`,`replayname`,`replayTime`) values (3,'hello?','2016152101','2020-04-07','hi!','李四','2020-04-07');

/*Table structure for table `notice` */

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `nid` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `upname` varchar(20) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `notice` */

insert  into `notice`(`nid`,`title`,`content`,`upname`,`time`) values (2,'通知','各位老师同学，请查看个人资料并更改/','张学友','2020-04-10');

/*Table structure for table `root` */

DROP TABLE IF EXISTS `root`;

CREATE TABLE `root` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rootid` varchar(20) NOT NULL,
  `rootname` varchar(20) NOT NULL,
  `rootpwd` varchar(25) NOT NULL,
  PRIMARY KEY (`rootid`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `root` */

insert  into `root`(`id`,`rootid`,`rootname`,`rootpwd`) values (1,'1111','张学友','12345'),(2,'222','小黑','54321');

/*Table structure for table `stu_info` */

DROP TABLE IF EXISTS `stu_info`;

CREATE TABLE `stu_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stuid` varchar(25) NOT NULL,
  `name` varchar(25) NOT NULL,
  `class_id` varchar(25) NOT NULL,
  `sex` varchar(4) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `faculty` varchar(25) NOT NULL,
  `loginpwd` varchar(25) NOT NULL,
  `activity` int(4) DEFAULT '0',
  `online` int(2) DEFAULT '0',
  `testscore` int(4) DEFAULT NULL,
  `expscore` int(4) DEFAULT NULL,
  `videoscore` int(4) DEFAULT NULL,
  `sumscore` int(4) DEFAULT NULL,
  PRIMARY KEY (`stuid`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `stu_info` */

insert  into `stu_info`(`id`,`stuid`,`name`,`class_id`,`sex`,`tel`,`faculty`,`loginpwd`,`activity`,`online`,`testscore`,`expscore`,`videoscore`,`sumscore`) values (1,'2016152101','小明','1','女','1234567765','计算机','1234567',0,0,0,0,0,0),(2,'2016152411','冰冰','1','女','11122224433','计算机','1234567',0,0,0,0,0,0);

/*Table structure for table `tea_info` */

DROP TABLE IF EXISTS `tea_info`;

CREATE TABLE `tea_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teaid` varchar(50) NOT NULL,
  `name` varchar(25) NOT NULL,
  `sex` varchar(4) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `job_title` varchar(25) NOT NULL,
  `faculty` varchar(25) NOT NULL,
  `login_pwd` varchar(25) NOT NULL,
  `class_id` varchar(25) NOT NULL,
  PRIMARY KEY (`teaid`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tea_info` */

insert  into `tea_info`(`id`,`teaid`,`name`,`sex`,`tel`,`job_title`,`faculty`,`login_pwd`,`class_id`) values (1,'123456','李四','男','13456789754','教授','计算机','123456','1'),(3,'345678','郭富城','男','12344567754','教授','计算机','123456','3');

/*Table structure for table `teaching_notice` */

DROP TABLE IF EXISTS `teaching_notice`;

CREATE TABLE `teaching_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `uname` varchar(25) NOT NULL,
  `uptime` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `teaching_notice` */

/*Table structure for table `upload_file_info` */

DROP TABLE IF EXISTS `upload_file_info`;

CREATE TABLE `upload_file_info` (
  `_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(50) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `pre_filename` varchar(100) NOT NULL,
  `now_filename` varchar(100) NOT NULL,
  `uptime` varchar(50) NOT NULL,
  `file_type` varchar(50) NOT NULL,
  PRIMARY KEY (`_file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `upload_file_info` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
