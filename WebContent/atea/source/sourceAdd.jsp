<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />
        
        <link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
		 <script>
        var count = 0;
        var outTime=1;//分钟


        try {
            var x = event.clientX;
            var y = event.clientY;
        } catch (e) {

        }
        //监听鼠标
        document.onmousedown = function () {
            var x1 = event.clientX;
            var y1 = event.clientY;
            if (x != x1 || y != y1) {
                count = 0;
            }
            x = x1;
            y = y1;
        };
 </script>
	</head>
	<body leftmargin="2" topmargin="9" background='/jsjwl/img/allbg.gif'>
			<form action="${pageContext.request.contextPath}/SourceServlet?method=addSource" name="formAdd" method="post"enctype="multipart/form-data" >
				     <table width="98%" align="center" border="0" cellpadding="4" cellspacing="1" bgcolor="#CBD8AC" style="margin-bottom:8px">
						<tr bgcolor="#EEF4EA">
					        <td colspan="3" background="/jsjwl/img/wbg.gif" class='title'><span>&nbsp;</span></td>
					    </tr>
						<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						         文件名称：
						    </td>
						    <td width="75%" bgcolor="#FFFFFF" align="left">
						        <input type="text" id="filename" name="filename" style="width: 360px;"/>
						    </td>
						</tr>
						<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						        附件上传：
						    </td>
						    <td width="75%" bgcolor="#FFFFFF" align="left">
						        <input type="file" name="attachmentOldName" id="attachmentOldName" style="width: 360px;"/>					        
						    </td>
						</tr>
						 <tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						       资料类型:
						    </td>
						  <td width="75%" bgcolor="#FFFFFF" align="left">						       
	                             <select class="INPUT_text" name="file_type"id="file_type"  style="height:20px;width:110px;">
									<option value="课件">课件</option>
						            <option value="教学大纲">教学大纲</option>
						            <option value="教学计划">教学计划</option>
						            <option value="教材">教材</option>	
							    </select>
						    </td>
						</tr>					
						<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						        &nbsp;
						    </td>
						    <td width="75%" bgcolor="#FFFFFF" align="left">
						       <input type="submit" value="提交" onclick="return check()"/>&nbsp; 
						       <input type="reset" value="重置"/>&nbsp;
						    </td>
						</tr>
					 </table>
			</form>
   </body>
<script>
function check(){
	//获取文件标题，介绍，非空检验
	var name=$("#filename").val();		
	//截取空格
	var nm=$.trim(name);	
	if(null==nm||""==nm){
		alert("标题不能为空");
		return false; //如果return false 会对表单进行提交
	}
	return true;
}
</script>   
</html>
