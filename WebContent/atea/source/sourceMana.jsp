<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />
		<link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />					
        <script>
        var count = 0;
        var outTime=1;//分钟


        try {
            var x = event.clientX;
            var y = event.clientY;
        } catch (e) {

        }
        //监听鼠标
        document.onmousedown = function () {
            var x1 = event.clientX;
            var y1 = event.clientY;
            if (x != x1 || y != y1) {
                count = 0;
            }
            x = x1;
            y = y1;
        };
 </script>
	</head>

	<body leftmargin="2" topmargin="2" background='/jsjwl/img/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="7" background="/jsjwl/img/tbg.gif">&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
					<td width="4%">序号</td>
					<td width="25%">名称</td>
					<td width="10%">附件</td>
					<td width="10%">类型</td>
					<td width="10%">上传者</td>
					<td width="10%">上传时间</td>
					<td width="10%">操作</td>
		        </tr>
				
			 <c:forEach items="${page.list}" var="s"  varStatus="status">
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						${status.index+1}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${s.filename}
					</td>					
					<td bgcolor="#FFFFFF" align="center">
						${s.pre_filename}
						&nbsp;&nbsp;&nbsp;
						<!-- <a href="#" onclick="down1('${s.pre_filename}')" style="font-size: 10px;color: red">down</a>
						 -->
					</td>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${s.file_type}
					</td>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${s.uname}
					</td>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${s.uptime}
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    <%--取消链接的默认行为 --%>
						<a href="javascript:void(0)" onclick="delSource(${s._file_id })"  class="pn-loperator">删除</a>
						
					</td>
				</tr>
			  </c:forEach>
			
			</table>
			<%@ include file="/jsp/pageFile.jsp" %>
			
			<table width='98%'  border='0'style="margin-top:8px;margin-left: 8px;">
			  <tr>
			  </tr>
		    </table>
	</body>
<script>
function delSource(sId){
	//做删除视频之前的确认提示
	if(confirm("确认删除当前的资料吗?")){
		//向服务端发起请求，将当前正在删除的视频的编号发送到服务端
		location.href="${pageContext.request.contextPath}/SourceServlet?method=deleteSourceByTeacher&id="+sId;
	}
}
</script>	
</html>




