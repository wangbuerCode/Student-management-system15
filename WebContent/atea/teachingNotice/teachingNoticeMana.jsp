<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />
		<link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />					
       
	</head>

	<body leftmargin="2" topmargin="2" background='/jsjwl/img/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="7" background="/jsjwl/img/tbg.gif">&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
					<td width="4%">序号</td>
					<td width="10%">上课时间</td>
					<td width="10%">上课地点</td>
					<td width="5%">发布者</td>
					<td width="10%">发布时间</td>
					<td width="10%">备注</td>
					<td width="4%">操作</td>
		        </tr>
				
			 <c:forEach items="${page.list}" var="tn"  varStatus="status">
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						${status.index+1}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.time}
					</td>					
				
					<td bgcolor="#FFFFFF" align="center">
						${tn.address}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.uname}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.uptime}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${tn.remark}
					</td>
					
					<td bgcolor="#FFFFFF" align="center">
					    <%--取消链接的默认行为 --%>
						<a href="javascript:void(0)" onclick="delTn(${tn.id })"  class="pn-loperator">删除</a>
						
					</td>
				</tr>
			  </c:forEach>
			
			</table>
			<%@ include file="/jsp/pageFile.jsp" %>
			
			<table width='98%'  border='0'style="margin-top:8px;margin-left: 8px;">
			  <tr>
			  </tr>
		    </table>
	</body>
<script>
function delTn(tnId){
	//做删除视频之前的确认提示
	if(confirm("确认删除当前的授课安排吗?")){
		//向服务端发起请求，将当前正在删除的视频的编号发送到服务端
		location.href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=deleteTeachingNoticeByTeacher&id="+tnId;
	}
}
</script>	
</html>




