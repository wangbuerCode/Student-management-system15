<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />

		<link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />
		
	</head>

	<body leftmargin="2" topmargin="2" background='/jsjwl/images/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
			
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="17" background="/jsjwl/images/tbg.gif">&nbsp;&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
				    <td width="4%">序号</td>
					<td width="10%">学号</td>
					<td width="10%">姓名</td>
					<td width="5%">班级</td>
					<td width="5%">性别</td>
					<td width="13%">电话</td>
					<td width="10%">系别</td>
					<td width="10%">活跃度</td>
					<td width="6%">测验分数</td>
					<td width="6%">实验与课设分数</td>
					<td width="6%">视频学习分数</td>
					<td width="6%">总分</td>
		        </tr>	
		        
		    <c:forEach items="${page.list}" var="stu"  varStatus="status">   
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						${status.index+1}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.stuid}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.name}
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    ${stu.class_id}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.sex}
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    ${stu.tel}
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    ${stu.faculty}
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    ${stu.activity}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.testscore}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.expscore}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.videoscore}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						${stu.sumscore}
					</td>
				</tr>
			  </c:forEach>
			</table>
			
			<%@ include file="/jsp/pageFile.jsp" %>
			
			
		
	</body>
<script>
function delStu(sid){
	//删除前的确认提示
	if(confirm("确认删除吗?")){
		window.location.href="/jsjwl/StuServlet?method=delStudentById&id="+sid;
	}
}
</script>
</html>