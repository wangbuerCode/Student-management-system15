<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />

		<link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />
		
		<script language="JavaScript" src="/jsjwl/js/public.js" type="text/javascript"></script>
		 <script>
        var count = 0;
        var outTime=1;//分钟


        try {
            var x = event.clientX;
            var y = event.clientY;
        } catch (e) {

        }
        //监听鼠标
        document.onmousedown = function () {
            var x1 = event.clientX;
            var y1 = event.clientY;
            if (x != x1 || y != y1) {
                count = 0;
            }
            x = x1;
            y = y1;
        };
 </script>
	</head>

	<body leftmargin="2" topmargin="2" background='/jsjwl/img/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="14" background="/jsjwl/img/tbg.gif">&nbsp;&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
					<td width="4%">序号</td>
					<td width="8%">标题</td>
					<td width="8%">发布时间</td>
		        </tr>	
			<c:forEach items="${page.list}" var="n"  varStatus="status"> 
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						 ${status.index+1}
					</td>
					<td bgcolor="#FFFFFF" align="center">
						<a href="${pageContext.request.contextPath}/NoticeServlet?method=findNoticeByNidByTeacher&id=${n.nid}">${n.title}</a>			
					</td>

					<td bgcolor="#FFFFFF" align="center">
						${n.time}
					</td>
			  </c:forEach>
				
				</tr>
			</table>
			<%@ include  file="/jsp/pageFile.jsp" %>
	</body>
</html>
