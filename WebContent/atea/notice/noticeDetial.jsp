<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />

		<link rel="stylesheet" type="text/css" href="/jsjwl/css/base.css" />
		
		<script language="JavaScript" src="/jsjwl/js/public.js" type="text/javascript"></script>
		 <script>
        var count = 0;
        var outTime=1;//分钟


        try {
            var x = event.clientX;
            var y = event.clientY;
        } catch (e) {

        }
        //监听鼠标
        document.onmousedown = function () {
            var x1 = event.clientX;
            var y1 = event.clientY;
            if (x != x1 || y != y1) {
                count = 0;
            }
            x = x1;
            y = y1;
        };
 </script>
	</head>

	<body leftmargin="2" topmargin="2" background='/jsjwl/img/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				 <tr>
					       <td align="center">标题：${notice.title}<hr/></td>
					    </tr>
					    
					   <tr>
					        <td align="center">内容：${notice.content}<hr/></td>
					    </tr>
					    <tr>
					        <td align="center">发布者：${notice.upname}<hr/></td>
					    </tr>
					    <tr>
					       <td align="center">发布时间:${notice.time}<hr/></td>
					    </tr>
					</table>
		  

	        <p class="return"><a href="${pageContext.request.contextPath}/NoticeServlet?method=findNoticeWithPageByTeacher&num=1">返回</a></p>
	
	</body>
</html>
