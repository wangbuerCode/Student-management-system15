<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
 
    <script src="/jsjwl/js/menu.js"></script>

	<style type="text/css">
		body {
			margin-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
		}
		.STYLE1 {
			color: #FFFFFF;
			font-weight: bold;
			font-size: 12px;
		}
		.STYLE2 {
			font-size: 12px;
			color: #03515d;
		}
		a:link {font-size:12px; text-decoration:none; color:#03515d;}
		a:visited{font-size:12px; text-decoration:none; color:#03515d;}
		.STYLE3 {font-size: 12px}
	</style>

  </head>

  <body>
     <table width="156"  border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top">
	            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td height="33" background="/jsjwl/images/main_21.gif">&nbsp;</td>
				      </tr>
				      <!-- caidan -->
				      <tr>
				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td1" onClick="show(1)">
					         <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                        <div align="center" class="STYLE1" style="font-family: 微软雅黑;">
					                        <div align="left"> &nbsp;&nbsp;&nbsp;我的信息</div>
					                      </div>
					                  </td>
					                </tr>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show1" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/TeacherServlet?method=findMyInfoUI" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;&nbsp;修改我的信息</a>
					            </td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
				      <!-- caidan -->
				      <tr>
                            <td height="30" background="/jsjwl/images/main_25.jpg" id="td2" onClick="show(2)">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                      <div align="center" class="STYLE1" style="font-family: 微软雅黑;">
					                        <div align="left"> &nbsp;&nbsp;&nbsp;班级管理</div>
					                      </div>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show2" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/StuServlet?method=findStudentByClass&num=1" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0"> &nbsp;班级人员信息</a>					           
					            </td>
					          </tr>
<%--					          <tr>--%>
<%--					            <td><a href="${pageContext.request.contextPath}/StuServlet?method=findOnlineStudentByClass&num=1" target="I2" style="margin-left: 20px;">--%>
<%--					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp; 学生学习情况</a>--%>
<%--					            </td>--%>
<%--					          </tr>--%>
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
				      <!-- caidan -->
				      <tr>
				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td3" onClick="show(3)">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                      <div align="center" class="STYLE1" style="font-family: 微软雅黑;">授课安排管理</div>
					                  </td>
					                </tr>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show3" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=findTeachingNoticeWithPage&num=1" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp; 授课安排管理</a>
					            </td>
					          </tr>
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/TeachingNoticeServlet?method=addTeachingNoticeUI" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;&nbsp;发布授课安排</a>
					            </td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
				      
				      
				      
				      <!-- caidan -->
				      <tr>
				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td4" onClick="show(4)">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                      <div align="center" class="STYLE1" style="font-family: 微软雅黑;">教学资料管理</div>
					                  </td>
					                </tr>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show4" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/SourceServlet?method=findSourceWithPageByTeacher&num=1" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;&nbsp;教学资料管理</a>					            </td>
					          </tr>
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/SourceServlet?method=addSourceUI" target="I2" style="margin-left: 20px;">
					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;上传教学资料</a>
					            </td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
				      
				      
				      
				      <!-- caidan -->
				      <tr>
				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td5" onClick="show(5)">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                        <div align="center" class="STYLE1" style="font-family: 微软雅黑;">
					                        <div align="left"> &nbsp;&nbsp;&nbsp;师生交流</div>
					                      </div>
					                  </td>
					                </tr>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show5" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td>
					            	<a href="${pageContext.request.contextPath}/MessageServlet?method=findMessagesWithPageByTeacher&num=1" target="I2" style="margin-left: 20px;">
					               <img src="${pageContext.request.contextPath}/images/arr3.gif" border="0">&nbsp;&nbsp; 查看学生留言</a>
					               </td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
				      
				      <tr>
				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td6" onClick="show(6)">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr>
					            <td width="13%">&nbsp;</td>
					            <td width="72%" height="20"><div align="center">
					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">
					                <tr>
					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>
					                  <td valign="middle">
					                      <div align="center" class="STYLE1" style="font-family: 微软雅黑;">
					                        <div align="left"> &nbsp;&nbsp;&nbsp;我的消息</div>
					                      </div>
					                  </td>
					                </tr>
					              </table>
					            </div></td>
					            <td width="15%">&nbsp;</td>
					          </tr>
					        </table>
					    </td>
				      </tr>
				      <tr id="show6" style="display:none">
				        <td align="center" valign="top">
					        <table border="0" align="center" cellpadding="5" cellspacing="5">
					          <tr>
					            <td><a href="${pageContext.request.contextPath}/NoticeServlet?method=findNoticeWithPageByTeacher&num=1" target="I2" style="margin-left: 20px;">
					               <img src="${pageContext.request.contextPath}/images/arr3.gif" border="0">&nbsp;&nbsp; 查看通知</a>
					         </tr>
					          
					        </table>
					    </td>
				      </tr>
				      <!-- caidan -->
<%--				      				      <tr>--%>
<%--				        <td height="30" background="/jsjwl/images/main_25.jpg" id="td7" onClick="show(7)">--%>
<%--					        <table width="100%" border="0" cellspacing="0" cellpadding="0">--%>
<%--					          <tr>--%>
<%--					            <td width="13%">&nbsp;</td>--%>
<%--					            <td width="72%" height="20"><div align="center">--%>
<%--					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">--%>
<%--					                <tr>--%>
<%--					                  <td><div align="center"><img src="/jsjwl/images/top_8.gif" width="16" height="16"></div></td>--%>
<%--					                  <td valign="middle">--%>
<%--					                      <div align="center" class="STYLE1" style="font-family: 微软雅黑;">--%>
<%--					                        <div align="left"> &nbsp;&nbsp;&nbsp;教学工作</div>--%>
<%--					                      </div>--%>
<%--					                  </td>--%>
<%--					                </tr>--%>
<%--					              </table>--%>
<%--					            </div></td>--%>
<%--					            <td width="15%">&nbsp;</td>--%>
<%--					          </tr>--%>
<%--					        </table>--%>
<%--					    </td>--%>
<%--				      </tr>--%>
<%--				      <tr id="show7" style="display:none">--%>
<%--				        <td align="center" valign="top">--%>
<%--					        <table border="0" align="center" cellpadding="5" cellspacing="5">--%>
<%--					          <tr>--%>
<%--					            <td><a href="/jsjwl/404.jsp"  target="1" style="margin-left: 20px;">--%>
<%--					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;&nbsp;在线测试</a>					            </td>--%>
<%--					          </tr>--%>
<%--					                    <tr>--%>
<%--					            <td><a href="/jsjwl/404.jsp"  target="1" style="margin-left: 20px;">--%>
<%--					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;过程性评价</a>--%>
<%--					            </td>--%>
<%--					          </tr>--%>
<%--					                    <tr>--%>
<%--					            <td><a href="/jsjwl/404.jsp"  target="1" style="margin-left: 20px;">--%>
<%--					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;教学视频</a>--%>
<%--					            </td>--%>
<%--					          </tr>--%>
<%--					                    <tr>--%>
<%--					            <td><a href="/jsjwl/404.jsp"  target="1" style="margin-left: 20px;">--%>
<%--					               <img src="/jsjwl/images/arr3.gif" border="0">&nbsp;实验与课程设计</a>--%>
<%--					            </td>--%>
<%--					          </tr>--%>
<%--					          --%>
<%--					       --%>
<%--					          --%>
<%--					          --%>
<%--					        </table>--%>
<%--					    </td>--%>
<%--				      </tr>--%>
				      <!-- caidan -->
				      <!-- meiyongde -->
<%--				      <tr>--%>
<%--				        <td height="10" background="/jsjwl/images/main_25.gif">--%>
<%--					        <table width="100%" border="0" cellspacing="0" cellpadding="0">--%>
<%--					          <tr>--%>
<%--					            <td width="13%">&nbsp;</td>--%>
<%--					            <td width="72%" height="20"><div align="center">--%>
<%--					              <table width="100%" height="21" border="0" cellpadding="0" cellspacing="0">--%>
<%--					                <tr>--%>
<%--					                  <td><div align="center"></div></td>--%>
<%--					                  <td valign="middle">--%>
<%--					                      --%>
<%--					                  </td>--%>
<%--					                </tr>--%>
<%--					              </table>--%>
<%--					            </div></td>--%>
<%--					            <td width="15%">&nbsp;</td>--%>
<%--					          </tr>--%>
<%--					        </table>--%>
<%--					    </td>--%>
<%--				      </tr>--%>
				      <!-- meiyongde -->
	            </table>
            </td>
        </tr>
     </table>
  </body>
</html>